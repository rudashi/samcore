Sam Core
================

This package contains various core elements for SAM.

![Totem.com.pl](https://www.totem.com.pl/wp-content/uploads/2016/06/logo.png)

## General System Requirements

- [PHP >7.4.0](http://php.net/)
- [Laravel ~6.*](https://github.com/laravel/framework)

## Quick Installation

If necessary, use the composer to download the library

```
$ composer require totem-it/sam-core
```

Remember to put repository in a composer.json

```
"repositories": [
    {
        "type": "vcs",
        "url":  "https://bitbucket.org/rudashi/samcore.git"
    }
],
```

## Features
- Bound route patterns:
  - id ```[0-9]+```
  - uuid ```[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}```
- Helper tait `\Totem\SamCore\App\Traits\HasAttribute` if you need to verify that attribute exists in model
- [Form request](#form-requests)
- [Repository Pattern](#repository)
- [Api Response](#api-response)
- [Api filtering](#filtering)
- [Api sorting](#sorting)
- [Api pagination](#pagination)
- [Api model relations](#relations)
- [File Handling](#file-upload)
- [Parameters](#parameters)
- [DataTable Filters](#dataTable-filters)
- [UUID](#uuid)
- [Enums](#enums)
- [CSV file creator/downloader](#csv)

## Usage

### Trait for ServiceProvider
Add trait `\Totem\SamCore\App\Traits\TraitServiceProvider` to your service provider file.

Use abstract method for namespace your package used to load and publishing
```php
public function getNamespace(): string
```
Helper for register middleware 
```php
$this->registerMiddlewareAlias($alias, $class);
```

Helper for register Eloquent Factory directory
```php
$this->registerEloquentFactoriesFrom($path);
```

Helper for mass class binding
```php
$this->configureBinding([ $interface => $class ]);
```

Helper for load and publish base components of package
```php
$this->loadAndPublish([
    string $json_translation = null, 
    string $migration = null, 
    string $blade_view = null
]);
```

### Form Requests
Extend your form request class with `\Totem\SamCore\App\Requests\BaseRequest`.

To use your own settings, publish purifier config.

```$ php artisan vendor:publish --provider="Mews\Purifier\PurifierServiceProvider"```

In order to clean the request from unnecessary HTML tags, use the method ```$this->setSanitizeFields(array)```

or fill class property `protected array $sanitizeFields = []`

### Controller
Extend yours controller with `\Totem\SamCore\App\Controllers\Controller`
or `\Totem\SamCore\App\Controllers\ApiController`

They have useful methods for [api response](#api-response) and [filtering requests](#filtering) 

### Repository
Extend yours repository interface with `\Totem\SamCore\App\Repositories\Contracts\RepositoryInterface`

For classes use `\Totem\SamCore\App\Repositories\BaseRepository`

### Blade directives
New directive is available for use within Blade templates.

```blade
@nl2br(`formatted text`)
```

```blade
@svg(`relative_public_file_path`, `classes [multiple with comma]`)
```

### API

Add to your .env for versioning your API
```dotenv
APP_API="1.0.0"
```

#### Api Response
To use Api responses, you must add trait `\Totem\SamCore\App\Traits\HasApi` to Controller 
or use a pre-defined controller `\Totem\SamCore\App\Controller\ApiController`

Examples:

Simple response for Eloquent Model utilizing ApiResource
```php
return $this->response($model);
```

Helper for response content not using Eloquent Model
```php
$this->success($content);
```

Helper utilizing error response
```php
$this->error(int $code, string $message);
```

#### Resources & Collection
Extend your api resources with `Totem\SamCore\App\Resources\ApiResource`

For collections use `Totem\SamCore\App\Resources\ApiCollection`

#### Filtering
Filtering can be implemented as a query parameter named for the field to be filtered on.
All filtered fields must be preceded by the phrase "search".

```
GET /users?search.active=1
```

Multiple filters result in an implicit AND.

```
GET /users?search.active=1&search.email=like:%com.pl
```

Search in relation field is also possible.

```
GET /users?search.role-name=admin
```

For queries that require non-simple equal comparisons, a colon and the use of one of the available comparisons must be required:
 
| Query | Description |
| --- | --- |
| ?search.field=1 | Equal (default) |
| ?search.field=eq:1 | Equal (alias) |
| ?search.field=neq:1 | Not Equal |
| ?search.field=gt:7 | Greater then |
| ?search.field=gte:7 | Greater or equal |
| ?search.field=lt:7 | Less then |
| ?search.field=lte:7 | Less or equal |
| ?search.field=null | Equal to NULL (MySQL equivalent: IS NULL) |
| ?search.field=-null | Equal to NULL (MySQL equivalent: IS NOT NULL) |
| ?search.field=in:1,3,5 | Group search for field (MySQL equivalent: WHERE IN) |
| ?search.field=nin:1,3,5 | Group search for field (MySQL equivalent: WHERE NOT IN) |
| ?search.field=like:%com.pl | Search for field (MySQL equivalent: LIKE) |
| ?search.field=nlike:%com.pl | Search for field (MySQL equivalent: NOT LIKE) |
| ?search.field=bt:7,10 | Between (MySQL equivalent BETWEEN) |

#### Sorting
To run a sorted search, you must pass the `sort` parameter. 
If the sort result has to be reversed, the field name should be preceded by a `minus`. 
If the result is to be sorted in more than one field, the parameter should be given a list of fields separated by commas.

```
GET /users?sort=lastname,-firstname
```

#### Pagination
Two parameters must be used to get a specific page of the result list:
* **offset**: Specifies the number of portions of returned data and is numbered from 0.
* **limit**: Specifies the number of elements on the page.

```
GET /users?limit=15&offset=0
```

#### Relations
To include model relations you must add `include` parameter. To attach more relations, names should be separated by commas.

```
GET /users?include=manager,department
```

It is possible to include nested relations, for that use dot expression.

```
GET /users?include=department.users
```

#### Count Relations
To include counter of relation, you must add `count` parameter.

```
GET /users?count=managers
```

### File Upload
For Models that need file support, add Trait
`\Totem\SamCore\App\Traits\HasFiles`

Routes
- Upload file : `POST /api/file`
- Remove file : `DELETE /api/file/{file-id}`

### Parameters 
For Models that need parametrizing column, add Trait
```\Totem\SamCore\App\Traits\HasParameters```

and create column `parameters` in database table of this model.

### DataTable Filters
Helper class to create well formatted api resource for data table in vuetify

```php
$table = new DataTableFilters();
$table->setDefaultView(string $view);
$table->setTypes(array $types);
$table->setViews(array $views);
$table->setStatus(array $status);
$table->addHeader(string $key, array $headers);
```

Header have multiple options:
```php
[
    'text' => '[translatable string]',
    'value' => '[searchable value]',
    'sortable' => '[optional bool]',
    'filterable' => '[optional bool]',
    'align' => '[optional string: start, end]',
    'customFilter' => '[optional bool]',
]
```

### UUID
Add Trait `\Totem\SamCore\App\Traits\HasUuid` to model for utilizing uuid 

### Enums
For Laravel < 8.0

Add Trait `\Totem\SamCore\App\Traits\CastsEnums` to model where you need to cast attribute as enum.

Create class property `protected array $enums = []` to define which attribute should be cast.

Example:
```php
protected array $enums = [
    'status' => StatusEnums::class,
];
```

If need it use trait `\Totem\SamCore\App\Traits\SelectableEnum` to create localized selectable array.

### CSV
You can download data as CSV (response object return)

```php
return (new \Totem\SamCore\App\Services\CSVService())
    ->streamDownload($collection, $filename, $http_header);
```
or 
```php
return \Totem\SamCore\App\Services\CSVService::download($collection, $filename, $http_header);
```

Ad default, the collection keys are used for header. It is possible to create custom headers
```php
$csv = new \Totem\SamCore\App\Services\CSVService();
$csv->setRowHeaders(['id', 'email']);
```

You can change basic CSV parameters like DELIMITER, ENCLOSURE, ESCAPE
```php
new \Totem\SamCore\App\Services\CSVService($separator, $enclosure, $escape);
```
or
```php
(new \Totem\SamCore\App\Services\CSVService())
    ->setDelimiter($separator)
    ->setEnclosure($enclosure)
    ->setEscape($escape);
```

## Authors

* **Borys Żmuda** - Lead designer - [LinkedIn](https://www.linkedin.com/in/boryszmuda/), [Portfolio](https://rudashi.github.io/)
