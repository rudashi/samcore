<?php

namespace Totem\SamCore\App\Controllers;

use Totem\SamCore\App\Traits\HasApi;
use Totem\SamCore\App\Traits\CanRequestQuery;

class ApiController extends Controller
{
    use HasApi,
        CanRequestQuery;

}
