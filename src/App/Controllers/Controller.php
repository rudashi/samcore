<?php

namespace Totem\SamCore\App\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use DispatchesJobs;

    /** @var mixed */
    protected $repository;

}
