<?php

namespace Totem\SamCore\App\Controllers;

use Totem\SamCore\App\Repositories\Contracts\FileInterfaceRepository;
use Totem\SamCore\App\Requests\FileUploadRequest;
use Totem\SamCore\App\Resources\FileResource;

class FileController extends ApiController
{

    public function __construct(FileInterfaceRepository $repository)
    {
        $this->repository = $repository;
    }

    public function upload(FileUploadRequest $request)
    {
        if (!$request->filled('path')) {
            return $this->response($this->error(400, __('The :attribute field is required.', ['attribute' => 'path'])));
        }
        return new FileResource(
            $this->repository->saveFile($request, $request->input('path'))
        );
    }

    public function remove(int $file_id): FileResource
    {
        return new FileResource(
            $this->repository->fileRemove($file_id)
        );
    }

}
