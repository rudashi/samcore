<?php

namespace Totem\SamCore\App\Exceptions;

use Symfony\Component\HttpKernel\Exception\HttpException;

class RepositoryException extends HttpException
{

    public function __construct(string $message = null, int $statusCode = 400, \Exception $previous = null, array $headers = array(), ?int $code = 0)
    {
        parent::__construct($statusCode, $message, $previous, $headers, $code);
    }

}