<?php

namespace Totem\SamCore\App\Model\Api;

use Illuminate\Database\Eloquent\RelationNotFoundException;
use Illuminate\Database\QueryException;
use Illuminate\Support\Collection;

class CollectionBuilder extends RequestQueryAbstract
{

    public function __construct(Collection $builder, array $default = [])
    {
        $this->query = $builder;
        $this->request = request();

        $this->setDefaults($default);
        $this->setRequestParameters();
    }

    public function filterRecords(): void
    {
    }

    public function ordering(): void
    {
        if (!$this->query->first() instanceof Collection) {
            $this->orderingCollection();
        }
    }

    public function paginate(): void
    {
        $this->paginateCollection();
    }

    public function includeRelations(): void
    {
    }

    public function make()
    {
        try {
            $this->filterRecords();
            $this->ordering();
            $this->paginate();
            $this->includeRelations();

            return $this->query;
        } catch (QueryException $exception) {
            return $this->throwError($exception);
        } catch (RelationNotFoundException $exception) {
            throw new \RuntimeException("Call to undefined relationship [$exception->relation]", $exception->getCode(), $exception->getPrevious());
        }
    }

}
