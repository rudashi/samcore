<?php

namespace Totem\SamCore\App\Model\Api\Contracts;

interface RequestQueryInterface
{

    public function filterRecords() : void;

    public function ordering() : void;

    public function paginate() : void;

    public function includeRelations() : void;

    public function make();

}
