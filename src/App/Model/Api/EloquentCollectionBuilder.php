<?php

namespace Totem\SamCore\App\Model\Api;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\RelationNotFoundException;
use Illuminate\Database\QueryException;
use RuntimeException;

class EloquentCollectionBuilder extends RequestQueryAbstract
{

    public function __construct(Collection $builder, array $default = [])
    {
        $this->query = $builder;
        $this->request = request();

        $this->setDefaults($default);
        $this->setRequestParameters();
    }

    public function filterRecords(): void
    {
        foreach ($this->filters as $filter) {
            $this->where($filter['column'], $this->getOperator($filter['operator']), $filter['value']);
        }
    }

    public function includeRelations(): void
    {
        if ($this->relations) {
            $this->query = $this->query->load($this->relations);
        }
    }

    public function ordering(): void
    {
        if ($this->sort) {
            if (version_compare(app()->version(), '8', '>')) {
                $this->query = $this->query->sortBy(array_map(static fn($k, $v) => [$k, $v], array_keys($this->sort), $this->sort));
            } else {
                foreach ($this->sort as $column => $direction) {
                    $this->query = $this->query->sortBy($column, SORT_REGULAR, $direction === 'desc');
                }
            }
        }
    }

    public function paginate(): void
    {
        $this->paginateCollection();
    }

    public function make()
    {
        try {
            $this->filterRecords();
            $this->includeRelations();
            $this->ordering();
            $this->paginate();
            $this->countRelations();

            return $this->query;
        } catch (QueryException $exception) {
            return $this->throwError($exception);
        } catch (RelationNotFoundException $exception) {
            throw new RuntimeException("Call to undefined relationship [$exception->relation]", $exception->getCode(), $exception->getPrevious());
        }
    }

    private function where(string $column, string $operator, $value): void
    {
        switch ($operator) {
            case QueryOperators::$in :
                $this->query = $this->query->whereIn($column, $value);
                break;
            case QueryOperators::$nin :
                $this->query = $this->query->whereNotIn($column, $value);
                break;
            case QueryOperators::$bt :
                if (!is_array($value) || count($value) !== 2) {
                    throw new RuntimeException('Missing values to search for ' . $column . '.');
                }
                if (strtotime($value[0])) {
                    $this->query = $this->query->where($column, QueryOperators::$gte, $value[0] . ' 00:00:00')
                        ->where($column, QueryOperators::$lte, $value[1] . ' 00:00:00');
                } else {
                    $this->query = $this->query->whereBetween($column, $value);
                }
                break;
            default:
                $this->query = $this->query->where($column, $this->getOperator($operator), $value);
                break;
        }
    }


}
