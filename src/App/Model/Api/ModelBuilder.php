<?php

namespace Totem\SamCore\App\Model\Api;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\RelationNotFoundException;

class ModelBuilder extends RequestQueryAbstract
{

    public function __construct(Model $builder, array $default = [])
    {
        $this->query = $builder;
        $this->request = request();

        $this->setDefaults($default);
        $this->setRequestParameters();
    }

    public function filterRecords(): void
    {
    }

    public function ordering(): void
    {
    }

    public function paginate(): void
    {
    }

    public function make(): ?Model
    {
        try {
            $this->includeRelations();

            return $this->query;
        } catch (RelationNotFoundException $exception) {
            throw new \RuntimeException("Call to undefined relationship [$exception->relation]", $exception->getCode(), $exception->getPrevious());
        }
    }

    public function includeRelations(): void
    {
        if ($this->relations) {
            $this->query->load($this->relations);
        }
    }

}
