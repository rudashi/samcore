<?php

namespace Totem\SamCore\App\Model\Api;

use Illuminate\Database\Eloquent\Relations\MorphTo;
use Illuminate\Database\QueryException;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\RelationNotFoundException;

class QueryBuilder extends RequestQueryAbstract
{

    public function __construct(Builder $builder, array $default = [])
    {
        $this->query = $builder;
        $this->request = request();

        $this->setDefaults($default);
        $this->setRequestParameters();
    }

    public function filterRecords(): void
    {
        foreach ($this->filters as $filter) {
            $relations = explode('.', $filter['column']);

            if (count($relations) > 1) {
                $column = end($relations);
                array_pop($relations);

                $this->whereRelation($relations, $column, $this->getOperator($filter['operator']), $filter['value']);
            } else {
                $this->where($this->query, $filter['column'], $this->getOperator($filter['operator']), $filter['value']);
            }
        }
    }

    private function where(Builder $query, string $column, string $operator, $value): void
    {
        switch ($operator) {
            case QueryOperators::$in :
                $query->whereIn($column, $value);
                break;
            case QueryOperators::$nin :
                $query->whereNotIn($column, $value);
                break;
            case QueryOperators::$bt :
                if (!is_array($value) || count($value) !== 2) {
                    throw new \RuntimeException('Missing values to search for '.$column.'.');
                }
                if (strtotime($value[0])) {
                    $query->whereDate($column, QueryOperators::$gte, $value[0])->whereDate($column, QueryOperators::$lte, $value[1]);
                } else {
                    $query->whereBetween($column, $value);
                }
                break;
            case QueryOperators::$null :
                $query->whereNull($column);
                break;
            case QueryOperators::$nnull :
                $query->whereNotNull($column);
                break;
            default:
                $query->where($column, $operator, $value);
                break;
        }
    }

    private function whereRelation(array $relation, string $column, string $operator, $value): void
    {
        if ($this->query->getRelation($relation[0]) instanceof MorphTo) {
            $this->query->whereHasMorph(implode('.', $relation), ['*'], function (Builder $query) use ($column, $operator, $value) {
                $this->where($query, $column, $operator, $value);
            });
        } else {
            $this->query->whereHas(implode('.', $relation), function (Builder $query) use ($column, $operator, $value) {
                $this->where($query, $column, $operator, $value);
            });
        }
    }

    public function ordering(): void
    {
        foreach ($this->sort as $column => $direction) {
            $this->query->orderBy($column, $direction);
        }
    }

    public function paginate(): void
    {
        if ($this->paginate['offset'] >= 0 && $this->paginate['limit'] >= 0) {
            $this->query->offset($this->paginate['offset']);
        }
        if ($this->paginate['limit'] >= 0) {
            $this->query->take($this->paginate['limit']);
        }
    }

    public function includeRelations(): void
    {
        $this->query->with($this->relations);
    }

    public function make()
    {
        try {
            $this->filterRecords();
            $this->ordering();
            $this->paginate();
            $this->includeRelations();

            return $this->query->get();
        } catch (QueryException $exception) {
            return $this->throwError($exception);
        } catch (RelationNotFoundException $exception) {
            throw new \RuntimeException("Call to undefined relationship [$exception->relation]", $exception->getCode(), $exception->getPrevious());
        }
    }

}
