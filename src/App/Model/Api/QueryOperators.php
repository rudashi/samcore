<?php

namespace Totem\SamCore\App\Model\Api;

class QueryOperators
{

    public static string $eq = '=';
    public static string $neq = '<>';
    public static string $gt = '>';
    public static string $gte = '>=';
    public static string $lt = '<';
    public static string $lte = '<=';
    public static string $like = 'like';
    public static string $nlike = 'not like';

    public static string $in = 'in';
    public static string $bt = 'between';
    public static string $nin = 'not in';

    public static string $null = 'IS NULL';
    public static string $nnull = 'IS NOT NULL';

}
