<?php

namespace Totem\SamCore\App\Model\Api;

use Illuminate\Support\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection as EloquentCollection;
use Totem\SamCore\App\Repositories\Contracts\RepositoryInterface;

class RequestQuery
{
    protected $api_request;

    public function __construct($collection, array $default = [])
    {
        switch (true) {
            case $collection instanceof RepositoryInterface :
                $this->api_request = new QueryBuilder($collection->getModel()->newQuery(), $default);
                break;
            case $collection instanceof EloquentCollection :
                $this->api_request = new EloquentCollectionBuilder($collection, $default);
                break;
            case $collection instanceof Collection :
                $this->api_request = new CollectionBuilder($collection, $default);
                break;
            case $collection instanceof Model :
                if ($collection->exists) {
                    $this->api_request = new ModelBuilder($collection, $default);
                } else {
                    $this->api_request = new QueryBuilder($collection->newQuery(), $default);
                }
                break;
            case $collection instanceof Builder :
                $this->api_request = new QueryBuilder($collection, $default);
                break;
            default:
                break;
        }
    }

    public function make()
    {
        return $this->api_request->make();
    }
}
