<?php

namespace Totem\SamCore\App\Model\Api;

use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Totem\SamCore\App\Model\Api\Contracts\RequestQueryInterface;

abstract class RequestQueryAbstract implements RequestQueryInterface
{
    protected array $paginate = [
        'offset' => -1,
        'limit' => -1,
    ];
    protected array $sort = [
        'id' => 'asc'
    ];
    protected array $filters = [];
    protected array $relations = [];
    protected array $relations_count = [];

    protected Request $request;

    /** @var mixed */
    protected $query;

    abstract public function filterRecords(): void;

    abstract public function ordering(): void;

    abstract public function paginate(): void;

    abstract public function includeRelations(): void;

    abstract public function make();

    protected function setDefaults(array $default): void
    {
        if (empty($default)) {
            return;
        }

        $this->setPaginate($default['paginate'] ?? $this->paginate);
        $this->setSort($default['sort'] ?? $this->sort);
        $this->setFilters($default['filters'] ?? $this->filters);
        $this->setRelations($default['relations'] ?? $this->relations);
        $this->setRelationsCount($default['relations_count'] ?? $this->relations_count);
    }

    protected function setPaginate(array $paginate): RequestQueryAbstract
    {
        $this->paginate = [
            'offset' => $paginate['offset'] ?? $this->paginate['offset'],
            'limit' => $paginate['limit'] ?? $this->paginate['limit'],
        ];

        return $this;
    }

    protected function setSort(array $sort): RequestQueryAbstract
    {
        $this->sort = $sort;

        return $this;
    }

    protected function setFilters(array $filters): RequestQueryAbstract
    {
        $this->filters = array_map(static function ($filter, $key) {
            if (is_array($filter)) {
                return [
                    'column' => $filter['column'],
                    'operator' => $filter['operator'] ?? 'eq',
                    'value' => stripslashes(preg_replace('/(?<!\\\\)[\"\']/', '', $filter['value'])),
                ];
            }
            if (strpos($filter, ':') !== false) {
                [$operator, $value] = explode(':', $filter);
                return [
                    'column' => str_replace('-', '.', $key),
                    'operator' => $operator,
                    'value' => array_map(static function ($item) {
                        return stripslashes(preg_replace('/(?<!\\\\)[\"\']/', '', $item));
                    },
                        preg_split('/(?:\'[^\']*\'|"[^"]*"|)\K(,|$)/', $value, null, PREG_SPLIT_NO_EMPTY)
                    )
                ];
            }
            if (in_array(strtolower($filter), ['null', '-null'], true)) {
                return [
                    'column' => str_replace('-', '.', $key),
                    'operator' => $filter === 'null' ? 'null' : 'nnull',
                    'value' => null,
                ];
            }
            return [
                'column' => str_replace('-', '.', $key),
                'operator' => 'eq',
                'value' => stripslashes(preg_replace('/(?<!\\\\)[\"\']/', '', $filter)),
            ];
        }, $filters, array_keys($filters));

        return $this;
    }

    protected function setRelations(array $relations = []): RequestQueryAbstract
    {
        $this->relations = $relations;

        return $this;
    }

    protected function setRelationsCount(array $relations = []): RequestQueryAbstract
    {
        $this->relations_count = $relations;

        return $this;
    }

    protected function setRequestParameters(): void
    {
        foreach (['include' => 'setRelations', 'count' => 'setRelationsCount'] as $key => $method) {
            if ($this->request->has($key)) {
                preg_match('/\[([^[]+)]/', $this->request->get($key), $relations);
                if (isset($relations[1])) {
                    $this->$method(explode(',', $relations[1]));
                } else {
                    $this->$method(explode(',', $this->request->get($key)));
                }
            }
        }

        if ($this->request->has('offset')) {
            $this->setPaginate(['offset' => $this->request->get('offset')]);
        }
        if ($this->request->has('limit')) {
            $this->setPaginate(['limit' => $this->request->get('limit')]);
        }
        if ($this->request->has('sort')) {
            $this->setSort($this->getSortArrayFromRequest($this->request->get('sort')));
        }
        $search = collect($this->request)->filter(static function ($value, $key) {
            return Str::startsWith($key, 'search') ?? $value;
        });
        if ($search->isNotEmpty()) {
            $this->addFilters($search->mapWithKeys(static function ($value, $key) {
                return [Str::after($key, '_') => $value];
            })->toArray());
        }
    }

    protected function addFilters(array $filters): RequestQueryAbstract
    {
        $this->setFilters(array_merge($this->filters, $filters));

        return $this;
    }

    protected function getOperator(string $operator = 'eq'): ?string
    {
        try {
            return QueryOperators::$$operator;
        } catch (\Throwable $exception) {
            return QueryOperators::$eq;
        }
    }

    protected function orderingCollection(): RequestQueryAbstract
    {
        $this->query = $this->query->sort(function ($a, $b) {
            foreach ($this->sort as $column => $direction) {
                if ($direction === 'desc') {
                    $first = $b;
                    $second = $a;
                } else {
                    $first = $a;
                    $second = $b;
                }

                if (is_array($first)) {
                    $cmp = strnatcmp($first[$column], $second[$column]);
                } else {
                    $cmp = strnatcmp($first->{$column}, $second->{$column});
                }

                if ($cmp !== 0) {
                    return $cmp;
                }
            }
            return 0;
        });

        return $this;
    }

    protected function paginateCollection(): RequestQueryAbstract
    {
        if ($this->paginate['offset'] >= 0 && $this->paginate['limit'] >= 0) {
            $this->query = $this->query->slice($this->paginate['offset'], $this->paginate['limit']);
        }
        if ($this->paginate['limit'] >= 0) {
            $this->query = $this->query->take($this->paginate['limit']);
        }

        return $this;
    }

    protected function countRelations(): void
    {
        if ($this->relations_count) {
            $this->query = $this->query->loadCount($this->relations_count);
        }
    }

    protected function throwError(QueryException $exception): string
    {
        if ($exception->getCode() === '42S22' && $exception->getPrevious()) {
            throw $exception;
        }
        return $exception->getMessage();
    }

    private function getSortArrayFromRequest(string $sort): array
    {
        return array_column(array_map(static function ($item) {
            if (strpos($item, '-') === 0) {
                return [substr($item, 1), 'desc'];
            }
            if (strpos($item, '+') === 0) {
                return [substr($item, 1), 'asc'];
            }
            return [$item, 'asc'];

        }, explode(',', $sort)), 1, 0);
    }

}
