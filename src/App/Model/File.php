<?php

namespace Totem\SamCore\App\Model;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

/**
 * @property int id
 * @property string name
 * @property string original_name
 * @property string extension
 * @property string attachable_type
 * @property int attachable_id
 * @property string type
 * @property \DateTime|null deleted_at
 */
class File extends Model
{
    use SoftDeletes;

    public function __construct(array $attributes = [])
    {
        $this->addHidden([
            'created_at',
            'deleted_at',
            'attachable_type',
            'attachable_id',
        ]);

        $this->fillable([
            'name',
            'original_name',
            'extension',
            'attachable_type',
            'attachable_id',
        ]);

        $this->append([
            'type',
        ]);

        parent::__construct($attributes);
    }

    public function attachable() : \Illuminate\Database\Eloquent\Relations\MorphTo
    {
        return $this->morphTo()->withoutGlobalScopes();
    }

    public function getTypeAttribute() : string
    {
        return $this->attributes['type'] = ($this->extension === 'pdf' ? $this->extension : 'image');
    }

}
