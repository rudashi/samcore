<?php

namespace Totem\SamCore\App\Repositories;

use Illuminate\Database\Eloquent\Builder;
use Totem\SamCore\App\Exceptions\RepositoryException;
use Totem\SamCore\App\Repositories\Contracts\RepositoryInterface;
use Totem\SamCore\App\Repositories\Contracts\SimpleRepositoryInterface;

abstract class BaseRepository implements RepositoryInterface, SimpleRepositoryInterface
{
    /**
     * @var \Illuminate\Database\Eloquent\Model|Builder
     */
    protected $model;

    public function __construct()
    {
        $this->makeModel();
    }

    abstract public function model() : string;

    public function setModel(string $modelClass)
    {
        $this->makeModel($modelClass);

        return $this;
    }

    protected function makeModel(string $className = null)
    {
        return $this->model = app()->make($className ?? $this->model());
    }

    public function getModel() : \Illuminate\Database\Eloquent\Model
    {
         return $this->model;
    }

    public function create(array $attributes = [])
    {
        return $this->model->create($attributes);
    }

    public function update(array $attributes, int $id): bool
    {
        $model = $this->find($id);

        return $model->update($attributes);
    }

    public function delete(int $id)
    {
        $model = $this->find($id);

        try {
            $model->delete();
            return $model;
        } catch (\Symfony\Component\HttpKernel\Exception\HttpException $exception) {
            throw new RepositoryException($exception->getMessage(), $exception->getStatusCode());
        } catch (\Exception $exception) {
            throw new RepositoryException($exception->getMessage());
        }
    }

    public function store(\Illuminate\Http\Request $request, int $id = 0)
    {
        // to declare
    }

    public function storeUUID(\Illuminate\Http\Request $request, string $uuid = null)
    {
        // to declare
    }

    public function all(array $columns = ['*'], string $orderBy = 'id', string $sortBy = 'asc') : \Illuminate\Database\Eloquent\Collection
    {
        return $this->model->orderBy($orderBy, $sortBy)->get($columns);
    }

    public function allWithRelations(array $relationships = [], array $columns = ['*'], string $orderBy = 'id', string $sortBy = 'asc') : \Illuminate\Database\Eloquent\Collection
    {
        return $this->model->with($relationships)->orderBy($orderBy, $sortBy)->get($columns);
    }

    public function find(int $id, array $columns = ['*'])
    {
        return $this->findWithRelationsById($id, [], $columns);
    }

    public function findBy(string $attribute, string $value, array $columns = ['*'])
    {
        return $this->model->where($attribute, '=', $value)->first($columns);
    }

    public function findById(int $id = 0, array $columns = ['*'])
    {
        return $this->find($id, $columns);
    }

    public function findByIds($ids, array $columns = ['*']) : \Illuminate\Database\Eloquent\Collection
    {
        return $this->model->findMany($ids, $columns);
    }

    public function findWithRelationsByIds($ids, array $relationships = [], array $columns = ['*']) : \Illuminate\Database\Eloquent\Collection
    {
        return $this->model->with($relationships)->findMany($ids, $columns);
    }

    public function findWithRelationsById(int $id = 0, array $relationships = [], array $columns = ['*'])
    {
        if ($id === 0) {
            throw new RepositoryException( __('No id have been given.') );
        }

        $data = $this->model->with($relationships)->find($id, $columns);

        if ($data === null) {
            throw new RepositoryException(  __('Given id :code is invalid or element not exist.', ['code' => $id]), 404);
        }

        return $data;
    }

    public function paginate(int $perPage = 15, array $columns = ['*']) : \Illuminate\Contracts\Pagination\LengthAwarePaginator
    {
        return $this->model->paginate($perPage, $columns);
    }

     public function paginateWithRelations(array $relationships = [], int $perPage = 15, array $columns = ['*']): \Illuminate\Contracts\Pagination\LengthAwarePaginator
     {
         return $this->model->with($relationships)->paginate($perPage, $columns);
     }

     public function with($relationships): Builder
     {
         return $this->model->with(\is_string($relationships) ? \func_get_args() : $relationships);
     }

     public function withCount($relationships): Builder
     {
         return $this->model->withCount(\is_string($relationships) ? \func_get_args() : $relationships);
     }

     public function activate(int $id, bool $toActivate = true, string $attribute = 'active')
     {
         $model = $this->find($id);
         $model->update( [$attribute => (int) $toActivate] );

         return $model;
     }

     public function deactivate(int $id, string $attribute = 'active')
     {
         return $this->activate( $id, false, $attribute);
     }

 }
