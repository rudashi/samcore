<?php

namespace Totem\SamCore\App\Repositories\Contracts;

use Illuminate\Http\UploadedFile;
use Totem\SamCore\App\Model\File;
use Totem\SamCore\App\Requests\FileUploadRequest;

interface FileInterfaceRepository
{

    public function upload(string $path, UploadedFile $file) : string;

    public function save(FileUploadRequest $request, string $path, string $type, string $uuid): File;

    public function saveFile(FileUploadRequest $request, $model): File;

    public function fileRemove(int $id, HasFilesInterface $model = null): File;

    public function findByModelUuid(string $uuid = null, string $model = null): \Illuminate\Database\Eloquent\Collection;

    public function firstByModelUuid(string $uuid = null, string $model = null): File;

}