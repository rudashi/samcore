<?php

namespace Totem\SamCore\App\Repositories\Contracts;

use Illuminate\Support\Collection;

interface HasFilesInterface
{

    public function getUuid(): string;

    public function path(): string;

    public function files(): \Illuminate\Database\Eloquent\Relations\MorphMany;

    public function attachFiles(array $files = []): iterable;

    public function getFilesLocalizationAttribute(): Collection;

    public function getFileInputConfig(): Collection;

}