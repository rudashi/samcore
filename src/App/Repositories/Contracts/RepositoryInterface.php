<?php

namespace Totem\SamCore\App\Repositories\Contracts;

use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

interface RepositoryInterface
{

    /**
     * @param   array   $attributes
     * @return  Model|$this
     */
    public function create(array $attributes = []);

    /**
     * @param   array   $attributes
     * @param   int     $id
     * @return  bool
     */
    public function update(array $attributes, int $id): bool;

    /**
     * @param   int     $id
     * @return  Collection|bool
     */
    public function delete(int $id);

    /**
     * @param   Request     $request
     * @param   int         $id
     * @return  mixed
     */
    public function store(Request $request, int $id = 0);

    /**
     * @param   Request     $request
     * @param   string|null $uuid
     * @return  mixed
     */
    public function storeUUID(Request $request, string $uuid = null);

    /**
     * @param   array   $columns
     * @param   string  $orderBy
     * @param   string  $sortBy
     * @return  Collection
     */
    public function all(array $columns = ['*'], string $orderBy = 'id', string $sortBy = 'asc'): Collection;

    /**
     * @param   array $relationships
     * @param   array $columns
     * @param   string $orderBy
     * @param   string $sortBy
     * @return  Collection
     */
    public function allWithRelations(array $relationships = [], array $columns = ['*'], string $orderBy = 'id', string $sortBy = 'asc'): Collection;

    /**
     * @param   int     $id
     * @param   array   $columns
     * @return  Collection|static[]
     */
    public function find(int $id, array $columns = ['*']);

    /**
     * @param   string  $attribute
     * @param   string  $value
     * @param   array   $columns
     * @return  Model|Collection|static[]
     */
    public function findBy(string $attribute, string $value, array $columns = ['*']);

    /**
     * @param   int $id
     * @param   array $columns
     * @return  Model|Collection|static[]
     */
    public function findById(int $id = 0, array $columns = ['*']);

    /**
     * @param   \Illuminate\Contracts\Support\Arrayable|array $ids
     * @param   array $columns
     * @return  Collection
     */
    public function findByIds($ids, array $columns = ['*']): Collection;

    /**
     * @param   \Illuminate\Contracts\Support\Arrayable|array $ids
     * @param   array   $relationships
     * @param   array   $columns
     * @return  Collection
     */
    public function findWithRelationsByIds($ids, array $relationships = [], array $columns = ['*']): Collection;

    /**
     * @param   int     $id
     * @param   array   $relationships
     * @param   array   $columns
     * @return  Collection|static[]
     */
    public function findWithRelationsById(int $id = 0, array $relationships = [], array $columns = ['*']);

    /**
     * @param   int     $perPage
     * @param   array   $columns
     * @return  LengthAwarePaginator
     * @throws  \InvalidArgumentException
     */
    public function paginate(int $perPage = 15, array $columns = ['*']): LengthAwarePaginator;

    /**
     * @param   array $relationships
     * @param   int $perPage
     * @param   array $columns
     * @return  LengthAwarePaginator
     */
    public function paginateWithRelations(array $relationships = [], int $perPage = 15, array $columns = ['*']): LengthAwarePaginator;

    /**
     * @param   string|array    $relationships
     * @return  Builder
     */
    public function with($relationships): Builder;

    /**
     * @param   string|array    $relationships
     * @return  Builder
     */
    public function withCount($relationships): Builder;

    /**
     * @param   int $id
     * @param   bool $toActivate
     * @param   string $attribute
     * @return  Collection|static[]
     */
    public function activate(int $id, bool $toActivate = true, string $attribute = 'active');

    /**
     * @param   int $id
     * @param   string $attribute
     * @return  Collection|static[]
     */
    public function deactivate(int $id, string $attribute = 'active');

}
