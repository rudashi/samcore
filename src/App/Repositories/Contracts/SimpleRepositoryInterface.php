<?php

namespace Totem\SamCore\App\Repositories\Contracts;

use Illuminate\Database\Eloquent\Model;

interface SimpleRepositoryInterface
{

    /**
     * Provide Model class name
     *
     * @return string
     */
    public function model(): string;

    /**
     * Get Current Model in repository
     *
     * @return Model
     */
    public function getModel(): Model;

    /**
     * Set Model to repository
     *
     * @param string $modelClass
     * @return mixed
     */
    public function setModel(string $modelClass);

}