<?php

namespace Totem\SamCore\App\Repositories;

use Totem\SamCore\App\Model\File;
use Totem\SamCore\App\Traits\CanFileUpload;
use Totem\SamCore\App\Exceptions\RepositoryException;
use Totem\SamCore\App\Repositories\Contracts\FileInterfaceRepository;

/**
 * @property \Illuminate\Database\Eloquent\Builder|File $model
 * @method File delete(int $id)
 */
class FileRepository extends BaseRepository implements FileInterfaceRepository
{

    use CanFileUpload;

    protected string $input_name = 'file_data';

    public function model(): string
    {
        return File::class;
    }

    public function findWithRelationsById(int $id = 0, array $relationships = [], array $columns = ['*']) : File
    {
        if ($id === 0) {
            throw new RepositoryException( __('File ID not provided.') );
        }

        /** @var File $data */
        $data = $this->model::withTrashed()->with($relationships)->find($id, $columns);

        if ($data === null) {
            throw new RepositoryException(  __('Given id :code is invalid or file not exist.', ['code' => $id]), 404);
        }

        if ($data->deleted_at !== null) {
            throw new RepositoryException( __('File with given id :code is deleted.', ['code' => $id]), 404);
        }

        return $data;
    }

    public function findByModelUuid(string $uuid = null, string $model = null): \Illuminate\Database\Eloquent\Collection
    {
        if ($uuid === null || $model === null) {
            throw new RepositoryException( __('Model UUID not provided.') );
        }

        return $this->model->where(['attachable_type' => $model, 'attachable_id' => $uuid])->get();
    }

    public function firstByModelUuid(string $uuid = null, string $model = null): File
    {
        $data = $this->findByModelUuid($uuid, $model);

        if ($data->isEmpty()) {
            throw new RepositoryException(  __('Given id :code is invalid or file not exist.', ['code' => $uuid]), 404);
        }
        return $data->first();
    }

}
