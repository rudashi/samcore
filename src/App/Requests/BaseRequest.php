<?php

namespace Totem\SamCore\App\Requests;

use Mews\Purifier\Facades\Purifier;
use Illuminate\Foundation\Http\FormRequest;

/**
 * @property int $id
 */
class BaseRequest extends FormRequest
{

    protected array $sanitizeFields = [];

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() : bool
    {
        return auth()->check();
    }

    public function validateResolved(): void
    {
        $this->sanitizeFields();

        parent::validateResolved();
    }

    public function setSanitizeFields(array $fields): BaseRequest
    {
        $this->sanitizeFields = $fields;

        return $this;
    }

    private function sanitizeFields(): void
    {
        $input = $this->all();

        foreach ($this->sanitizeFields as $field) {
            if (isset($input[$field])) {
                $input[$field] = Purifier::clean($input[$field]);
            }
        }
        $this->replace($input);
    }

}
