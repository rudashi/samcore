<?php

namespace Totem\SamCore\App\Requests;

class FileUploadRequest extends BaseRequest
{

    public function rules() : array
    {
        return [
            'file_data' => 'required|max:5000|mimes:jpeg,png,jpg,gif,pdf',
            'path'      => 'string',
        ];
    }

    public function attributes() : array
    {
        return [
            'file_data' => '`file_data`',
        ];
    }
}