<?php

namespace Totem\SamCore\App\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class ApiCollection extends ResourceCollection
{

    public function __construct($resource, string $collects = null)
    {
        if (!$this->collects && $collects !== null) {
            $this->collects = $collects;
        }

        parent::__construct($resource);
    }

    public function with($request) : array
    {
        return [
            'apiVersion' => env('APP_API', '1.0.0')
        ];
    }

}
