<?php

namespace Totem\SamCore\App\Resources;

use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\MessageBag;
use Illuminate\Http\Resources\MissingValue;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @property bool preserveKeys
 */
class ApiResource extends JsonResource
{

    public function __construct($resource)
    {
        self::wrap('data');

        parent::__construct($resource);
    }

    public static function collection($resource): ApiCollection
    {
        return tap(new ApiCollection($resource, static::class), static function ($collection) {
            if (property_exists(static::class, 'preserveKeys')) {
                $collection->preserveKeys = (new static([]))->preserveKeys === true;
            }
        });
    }

    public function toArray($request): array
    {
        if ($this->resource instanceof MessageBag) {

            self::wrap('error');

            return [
                'code' => (int) $this->resource->first('code'),
                'message' => $this->resource->first('message'),
            ];
        }
        if (is_bool($this->resource)) {
            return [];
        }
        return parent::toArray($request);
    }

    public function with($request): array
    {
        return [
            'apiVersion' => env('APP_API', '1.0.0')
        ];
    }

    public function withResponse($request, $response): void
    {
        if ($this->resource instanceof MessageBag) {
            $response->setStatusCode($this->resource->first('code'));
        }
        if (is_bool($this->resource)) {
            $response->setStatusCode(Response::HTTP_NO_CONTENT);
        }
    }

    protected function makeSimplifyCollection(Request $request, string $resourceClass, string $relation)
    {
        /** @var ApiResource $resourceClass */
        $collection = $resourceClass::collection($this->whenLoaded($relation));

        if (!$collection instanceof MissingValue
            && $collection->collection !== null
            && $request->has('simplify')
            && in_array($relation, explode(',', $request->get('simplify')), true)
        ) {
            return $collection->collection->pluck('id');
        }
        return $collection;
    }

    protected function getNumberFormat(?float $number, int $decimals = 2): ?string
    {
        return $number !== null ? number_format($number, $decimals, '.', '') : null;
    }

}
