<?php

namespace Totem\SamCore\App\Resources;

class FileCollection extends ApiCollection
{

    public $collects = FileResource::class;

}
