<?php

namespace Totem\SamCore\App\Resources;

/**
 * @property \Totem\SamCore\App\Model\File resource
 */
class FileResource extends ApiResource
{

    public function toArray($request) : array
    {
        return [
            'id'                => $this->resource->id,
            'name'              => $this->resource->name,
            'type'              => $this->resource->type,
            'original_name'     => $this->resource->original_name,
            'extension'         => $this->resource->extension,
            'attachable_type'   => $this->resource->attachable_type,
            'attachable_id'     => $this->resource->attachable_id,
            'preview'           => asset('storage/' .$this->resource->name)
        ];
    }

}
