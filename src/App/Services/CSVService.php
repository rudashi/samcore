<?php

namespace Totem\SamCore\App\Services;

use SplFileObject;
use Illuminate\Support\Enumerable;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\StreamedResponse;

class CSVService
{
    public const DELIMITER = ';';
    public const ENCLOSURE = '"';
    public const ESCAPE = '\\';

    protected string $delimiter = self::DELIMITER;
    protected string $enclosure = self::ENCLOSURE;
    protected string $escape = self::ESCAPE;
    protected array $rowHeaders = [];
    protected ?SplFileObject $file;

    public function __construct(string $delimiter = null, string $enclosure = null, string $escape = null)
    {
        $this->delimiter    = $delimiter ?? self::DELIMITER;
        $this->enclosure    = $enclosure ?? self::ENCLOSURE;
        $this->escape       = $escape ?? self::ESCAPE;
    }

    public function setDelimiter(string $delimiter): self
    {
        $this->delimiter = $delimiter;

        return $this;
    }

    public function setEnclosure(string $enclosure): self
    {
        $this->enclosure = $enclosure;

        return $this;
    }

    public function setEscape(string $escape): self
    {
        $this->escape = $escape;

        return $this;
    }

    public function setRowHeaders(array $rowHeaders): self
    {
        $this->rowHeaders = $rowHeaders;

        return $this;
    }

    public function streamDownload($collection, string $filename, $headers = []): StreamedResponse
    {
        return response()->streamDownload(function () use ($collection) {
            $this->openFileObject('php://output');
            $this->setHeader($collection);
            $this->setRows($collection);
            $this->closeFileObject();
        },
            self::setFileName($filename),
            array_merge([
                'Content-Type'  => 'text/csv',
            ], $headers)
        );
    }

    public static function download($collection, string $filename, $headers = []): BinaryFileResponse
    {
        return response()->download(
            (new static)->openFileObject($filename)->setHeader($collection)->setRows($collection)->getFile(),
            self::setFileName($filename),
            array_merge([
                'Content-Type'  => 'text/csv',
            ], $headers)
        );
    }

    private function openFileObject(string $filename): self
    {
        $this->file = new SplFileObject($filename, 'w');

        return $this;
    }

    private function closeFileObject(): self
    {
        $this->file = null;

        return $this;
    }

    private function setHeader($collection): self
    {
        if (empty($this->rowHeaders)) {
            $headers = [];
            if ($collection instanceof Enumerable) {
                $headers = $this->flattenRow($collection->first());
            }
            if  (is_array($collection)) {
                $headers = $collection[0];
            }
            $this->setRowHeaders(array_keys($headers));

        }
        return $this->setRow($this->rowHeaders);
    }

    private function setRows($collection): self
    {
        if (!$collection instanceof Enumerable && !is_array($collection)) {
            throw new \RuntimeException('Non-Iterable Object.');
        }
        foreach ($collection as $entry) {
            $this->setRow(array_values($this->flattenRow($entry)));
            unset($entry);
        }

        return $this;
    }

    private function setRow(array $line): self
    {
        $this->file->fputcsv($line, $this->delimiter, $this->enclosure, $this->escape);

        return $this;
    }

    private function flattenRow($entry): array
    {
        return method_exists($entry, 'toArray') ? $entry->toArray() : (array) $entry;
    }

    private function getFile(): SplFileObject
    {
        return $this->file;
    }

    private static function setFileName(string $filename): string
    {
        return "$filename.csv";
    }

}
