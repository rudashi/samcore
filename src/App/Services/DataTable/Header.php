<?php

namespace Totem\SamCore\App\Services\DataTable;

use Rudashi\Profis\Classes\DataTransferObject;

class Header extends DataTransferObject
{

    public string $text;
    public string $value;
    public ?string $hide = null;
    public ?string $description = null;
    public ?bool $translate = null;
    public ?bool $sortable = null;
    public ?bool $filterable = null;
    public ?string $align = null;
    public ?string $customFilter = null;


    public function toArray(): array
    {
        return array_filter(parent::toArray(), static function ($v) {
            return $v !== null;
        });
    }

}
