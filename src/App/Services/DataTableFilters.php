<?php

namespace Totem\SamCore\App\Services;

use Illuminate\Support\Collection;
use Totem\SamCore\App\Services\DataTable\Header;

class DataTableFilters extends Parameters
{

    public function __construct(string $defaultView = null)
    {
        parent::__construct(['defaultView' => $defaultView]);
    }

    public function setDefaultView($view): DataTableFilters
    {
        $this->setProperty('defaultView', $view);
        return $this;
    }

    public function setTypes($types): DataTableFilters
    {
        $this->setProperty('types', $types);
        return $this;
    }

    public function setViews($views): DataTableFilters
    {
        $this->setProperty('views', $views);

        if (count($views) > 0 && $this->getProperty('defaultView') === null) {
            $this->setDefaultView(array_key_first($views));
        }
        return $this;
    }

    public function setStatus($status): DataTableFilters
    {
        $this->setProperty('status', $status);
        return $this;
    }

    public function addHeader(string $key, array $headers): DataTableFilters
    {
        if ($this->validateHeaderView($key) === false) {
            throw new \UnexpectedValueException('View key not register.');
        }

        $this->setProperty('header', array_merge(
            $this->getProperty('header') ?? [],
            [$key => collect($headers)->map(function(array $header) {
                return new Header($header);
            })->toArray()]
        ));

        return $this;
    }

    private function validateHeaderView(string $key): bool
    {
        if (is_array($this->getProperty('views')) && array_key_exists($key, $this->getProperty('views'))) {
            return true;
        }
        if ($this->getProperty('views') instanceof Collection && $this->getProperty('views')->has($key)) {
            return true;
        }
        if (is_object($this->getProperty('views')) && property_exists($this->getProperty('views'), $key)) {
            return true;
        }
        return false;
    }

}
