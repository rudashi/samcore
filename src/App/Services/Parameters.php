<?php

namespace Totem\SamCore\App\Services;

use JsonSerializable;
use Totem\SamCore\App\Traits\Jsonable;
use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Contracts\Support\Jsonable as Base;

class Parameters implements Base, Arrayable, JsonSerializable
{
    use Jsonable;

    protected array $properties = [];

    public function __construct(array $properties = null)
    {
        $this->fill($properties);
    }

    public function fill(array $properties) : self
    {
        foreach ($properties as $key => $value) {
            $this->setProperty($key, $value);
        }

        return $this;
    }

    public function setProperty($key, $value) : self
    {
        $this->properties[$key] = $value;

        return $this;
    }

    public function getProperty($key)
    {
        return $this->properties[$key] ?? null;
    }

    public function propertyExists($property) : bool
    {
        return $this->getProperty($property) !== null;
    }

    public function unsetProperty($property) : void
    {
        unset($this->properties[$property]);
    }

    public function isEmpty(): bool
    {
        return empty($this->properties);
    }

    public function isNotEmpty(): bool
    {
        return ! $this->isEmpty();
    }

    public function __get($key)
    {
        return $this->getProperty($key);
    }

    public function __set($key, $value) : void
    {
        $this->setProperty($key, $value);
    }

    public function __isset($key) : bool
    {
        return $this->propertyExists($key);
    }

    public function __unset($key) : void
    {
        $this->unsetProperty($key);
    }

    public function toArray(): array
    {
        return array_map(static function ($value) {
            return $value instanceof Arrayable ? $value->toArray() : $value;
        }, $this->properties);
    }

    public function jsonSerialize() : array
    {
        return $this->toArray();
    }

}
