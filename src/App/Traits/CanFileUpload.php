<?php

namespace Totem\SamCore\App\Traits;

use Illuminate\Http\UploadedFile;
use Illuminate\Support\Str;
use Totem\SamCore\App\Exceptions\RepositoryException;
use Totem\SamCore\App\Model\File;
use Totem\SamCore\App\Repositories\Contracts\HasFilesInterface;
use Totem\SamCore\App\Requests\FileUploadRequest;

trait CanFileUpload
{

    public function attachToModel(File $file, HasFilesInterface $model): File
    {
        $model->files()->save($file);

        return $file;
    }

    public function createModel(UploadedFile $uploadedFile, string $imageName): File
    {
        return $this->model->fill([
            'name' => $imageName,
            'original_name' => $uploadedFile->getClientOriginalName(),
            'extension' => $uploadedFile->extension(),
        ]);
    }

    public function fileRemove(int $id, HasFilesInterface $model = null): File
    {
        if ($model instanceof HasFilesInterface) {
            if ($this->model->where(['id' => $id, 'attachable_id' => $model->getUuid()])->count()) {
                return $this->delete($id);
            }
            throw new RepositoryException(__('File with given id :code is not assigned.', ['code' => $id]), 422);
        }
        return $this->delete($id);
    }

    public function save(FileUploadRequest $request, string $path, string $type, string $uuid): File
    {
        $uploadedFile = $request->file($this->input_name);

        $file = $this->model->fill([
            'name' => $this->upload($path, $uploadedFile),
            'original_name' => $uploadedFile->getClientOriginalName(),
            'extension' => $uploadedFile->extension(),
            'attachable_type' => $type,
            'attachable_id' => $uuid,
        ]);
        $file->save();
        return $file;
    }

    public function saveFile(FileUploadRequest $request, $model): File
    {
        $uploadedFile = $request->file($this->input_name);

        if ($model instanceof HasFilesInterface) {
            return $this->saveAndAttach(
                $uploadedFile,
                $this->upload($model->path(), $uploadedFile),
                $model
            );
        }
        $file = $this->createModel($uploadedFile, $this->upload($model, $uploadedFile));
        $file->save();

        return $file;
    }

    public function saveAndAttach(UploadedFile $uploadedFile, string $imageName, HasFilesInterface $model): File
    {
        return $this->attachToModel(
            $this->createModel($uploadedFile, $imageName),
            $model
        );
    }

    public function upload(string $path, UploadedFile $file): string
    {
        return $file->storeAs($path, Str::random(35) . '.' . $file->extension(), ['disk' => 'public']);
    }

}
