<?php

namespace Totem\SamCore\App\Traits;

use Totem\SamCore\App\Model\Api\RequestQuery;

trait CanRequestQuery
{

    public function addRequestQuery($model = null, array $default = []): RequestQuery
    {
        return new RequestQuery($model ?? $this->repository, $default);
    }

    public function getFromRequestQuery($model = null, array $default = [])
    {
        return $this->addRequestQuery($model, $default)->make();
    }

}
