<?php

namespace Totem\SamCore\App\Traits;

use BenSampo\Enum\Enum;
use BenSampo\Enum\Exceptions\InvalidEnumKeyException;

/**
 * @property array $enums;
 */
trait CastsEnums
{

    protected function isEnumAttribute(string $key) : bool
    {
        return isset($this->enums[$key]);
    }

    private function getEnumKey(string $key): ?Enum
    {
        /** @var Enum $class */
        $class = $this->enums[$key];

        if ($this->getAttributeFromArray($key) !== null) {
            try {
                return $class::fromKey($this->getAttributeFromArray($key));
            } catch (InvalidEnumKeyException $e) {
                return $class::fromValue($this->getAttributeFromArray($key));
            }
        }
        return null;
    }

    public function getAttributeValue($key)
    {
        if ($this->isEnumAttribute($key)) {
            return $this->getEnumKey($key);
        }

        return parent::getAttributeValue($key);
    }

    public function getAttribute($key)
    {
        if ($this->isEnumAttribute($key)) {
            return $this->getAttributeValue($key);
        }
        return parent::getAttribute($key);
    }

}
