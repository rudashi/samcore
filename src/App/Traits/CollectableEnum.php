<?php

namespace Totem\SamCore\App\Traits;

use Illuminate\Support\Collection;

trait CollectableEnum
{

    public static function toCollection(): Collection
    {
        $array = self::toArray();
        $collection = collect();

        foreach ($array as $key => $value) {
            $collection->push(
                (object)[
                    'value' => $value,
                    'key' => $key,
                    'description' => self::getDescription($value)
                ]
            );
        }

        return $collection;
    }

}
