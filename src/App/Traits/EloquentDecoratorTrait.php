<?php

namespace Totem\SamCore\App\Traits;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Arr;

trait EloquentDecoratorTrait
{

    public function decorator(string $column, array $classMap = [], $attributes = null, $connection = null): Model
    {
        $entryClassName = Arr::get((array) $attributes, $column);

        return $this->decorateBuilder(Arr::get($classMap, $entryClassName ?? '', $entryClassName ?? ''), true, $attributes, $connection);
    }

    public function decorateBuilder(string $entryClassName, bool $is_subclass = true, $attributes = null, $connection = null): Model
    {
        if (class_exists($entryClassName) && ($is_subclass === false || ($is_subclass === true && is_subclass_of($entryClassName, self::class)))) {
            $model = new $entryClassName;
        } else {
            $model = $this->newInstance();
        }

        /** @var Model $model */
        $model->exists = true;
        $model->setRawAttributes((array) $attributes, true);
        $model->setConnection($connection ?: $this->getConnectionName());
        $model->fireModelEvent('retrieved', false);

        return $model;
    }

    public function newFromBuilder($attributes = [], $connection = null): Model
    {
        return $this->decorator('type', [], $attributes, $connection);
    }

}
