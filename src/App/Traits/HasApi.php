<?php

namespace Totem\SamCore\App\Traits;

use Illuminate\Support\Collection;
use Illuminate\Support\MessageBag;
use Totem\SamCore\App\Resources\ApiCollection;
use Totem\SamCore\App\Resources\ApiResource;

trait HasApi
{

    public function error(int $code, string $message): MessageBag
    {
        return new MessageBag([
            'code' => $code,
            'message' => $message
        ]);
    }

    public function success($response): Collection
    {
        if ($response instanceof Collection) {
            return $response;
        }
        return collect($response);
    }

    /**
     * @param mixed $response
     * @param string|null $class
     * @return ApiResource|ApiCollection
     */
    public function response($response, string $class = null)
    {
        if ($class !== null) {
            return new $class($response);
        }
        return new ApiResource($response);
    }

}
