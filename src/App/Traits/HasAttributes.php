<?php

namespace Totem\SamCore\App\Traits;

trait HasAttributes
{
    public function hasAttribute($key): bool
    {
        return array_key_exists($key, $this->getAttributes());
    }
}
