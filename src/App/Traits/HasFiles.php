<?php

namespace Totem\SamCore\App\Traits;

use Illuminate\Support\Collection;
use Totem\SamCore\App\Model\File;

/**
 * @property string|int $id
 * @property \Illuminate\Database\Eloquent\Collection|File[] files
 * @property \Illuminate\Database\Eloquent\Collection|string[] filesLocalization
 */
trait HasFiles
{

    public function getUuid(): string
    {
        return $this->id;
    }

    public function attachFiles(array $files = []): iterable
    {
        return $this->files()->saveMany(File::findMany($files));
    }

    public function files(): \Illuminate\Database\Eloquent\Relations\MorphMany
    {
        return $this->morphMany(File::class, 'attachable');
    }

    public function getFilesLocalizationAttribute(): Collection
    {
        return $this->files->map(static function (File $file) {
            return asset('storage/' . $file->name);
        });
    }

    public function getFileInputConfig(): Collection
    {
        return new Collection([
            'initialPreview' => $this->filesLocalization,
            'initialPreviewConfig' => $this->files->map(static function (File $file) {
                return [
                    'previewAsData' => true,
                    'type' => $file->type,
                    'caption' => $file->original_name,
                    'key' => $file->id,
                ];
            }),
        ]);
    }

}
