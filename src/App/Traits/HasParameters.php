<?php

namespace Totem\SamCore\App\Traits;

use Totem\SamCore\App\Services\Parameters;

/**
 * @mixin \Illuminate\Database\Eloquent\Model|\Illuminate\Database\Eloquent\Builder
 */
trait HasParameters
{

    protected static array $parameters = [];

    protected static string $parameters_column = 'parameters';

    protected static bool $parameters_intersect = true;

    public static function getParameters(): Parameters
    {
        return new Parameters(array_fill_keys(static::$parameters, null));
    }

    public function getParametersAttribute(): Parameters
    {
        $base = array_fill_keys(static::$parameters, null);
        $parameters = $this->attributes[static::$parameters_column] ?? null;

        return new Parameters($parameters
            ? array_merge($base, static::$parameters_intersect
                ? array_intersect_key($this->fromJson($parameters), $base)
                : $this->fromJson($parameters)
            )
            : $base
        );
    }

    public function setParametersIntersect(bool $value = true): self
    {
        self::$parameters_intersect = $value;

        return $this;
    }

}
