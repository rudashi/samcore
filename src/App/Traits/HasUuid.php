<?php

namespace Totem\SamCore\App\Traits;

trait HasUuid
{

    public static function bootHasUuid(): void
    {
        $callback = static function(\Illuminate\Database\Eloquent\Model $model) {
            $model->setKeyType('string');
            $model->setIncrementing(false);

            if (! $model->getKey()) {
                $model->{$model->getKeyName()} = static::generateUuid();
            }

        };

        static::creating($callback);
        static::retrieved($callback);
    }

    public static function generateUuid(): string
    {
        return \Illuminate\Support\Str::uuid()->toString();
    }

}
