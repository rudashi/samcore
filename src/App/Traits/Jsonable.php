<?php

namespace Totem\SamCore\App\Traits;

use Illuminate\Database\Eloquent\JsonEncodingException;

trait Jsonable
{

    public function toArray(): array
    {
        return get_object_vars($this);
    }

    public function toJson($options = 0) : string
    {
        $json = json_encode($this->jsonSerialize(), $options);

        if (JSON_ERROR_NONE !== json_last_error()) {
            throw JsonEncodingException::forModel($this, json_last_error_msg());
        }

        return $json;
    }

    public function jsonSerialize() : array
    {
        return get_object_vars($this);
    }

    public function __toString()
    {
        return $this->toJson();
    }

}
