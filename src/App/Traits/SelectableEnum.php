<?php

namespace Totem\SamCore\App\Traits;

trait SelectableEnum
{

    public static function toSelect(): array
    {
        $array = static::toArray();
        $selectArray = [];

        foreach ($array as $value) {
            $selectArray[] = [
                'text' => static::getDescription($value),
                'value' => $value
            ];
        }

        return $selectArray;
    }

}
