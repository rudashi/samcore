<?php

namespace Totem\SamCore\App\Traits;

use Illuminate\Contracts\Container\BindingResolutionException;

trait TraitServiceProvider
{
    /**
     * Namespace for package.
     *
     * @return string
     */
    abstract public function getNamespace(): string;

    protected function registerEloquentFactoriesFrom(string $path): void
    {
        try {
            $this->app->make(\Illuminate\Database\Eloquent\Factory::class)->load($path);
        } catch (BindingResolutionException $e) {
        }
    }

    protected function registerMiddlewareAlias(string $alias, string $class = ''): void
    {
        $this->getRouter()->aliasMiddleware($alias, $class);
    }

    protected function registerRoutePattern(string $key, string $pattern): void
    {
        $this->getRouter()->pattern($key, $pattern);
    }

    protected function configureBinding(array $bindings): void
    {
        foreach ($bindings as $abstract => $concrete) {
            $this->app->bind($abstract, $concrete);
        }
    }

    protected function isWebProject(): bool
    {
        return config('frontend-type') === 'web';
    }

    protected function setDefaultDriver(string $name): void
    {
        /** @var \Illuminate\Auth\AuthManager $auth */
        $auth = $this->app['auth'];
        $auth->setDefaultDriver($name);
    }

    protected function extendAuthGuard(string $path): void
    {
        $this->mergeConfigFrom($path, 'auth.guards');
    }

    protected function extendAuthProvider(string $path): void
    {
        $this->mergeConfigFrom($path, 'auth.providers');
    }

    protected function extendAuthPassword(string $path): void
    {
        $this->mergeConfigFrom($path, 'auth.passwords');
    }

    protected function extendAuthDefault(string $path): void
    {
        $this->mergeConfigFrom($path, 'auth.defaults');
    }

    protected function replaceConfigFrom(string $path, string $key): void
    {
        $config = $this->app['config']->get($key, []);

        $this->app['config']->set($key, array_replace($config, require $path));
    }

    protected function loadAndPublish(string $language = null, string $migration = null, string $view = null): void
    {
        if ($language) {
            $this->loadJsonTranslationsFrom($language);
            $this->publishes([
                $language => resource_path('lang/vendor/'.$this->getNamespace()),
            ], $this->getNamespace().'-lang');
        }
        if ($migration) {
            $this->loadMigrationsFrom($migration);
            $this->publishes([
                $migration => database_path('migrations'),
            ], $this->getNamespace().'-migrations');
        }
        if ($view) {
            $this->loadViewsFrom($view, $this->getNamespace());
            $this->publishes([
                $view => resource_path('views/vendor/'.$this->getNamespace()),
            ], $this->getNamespace().'-views');
        }
    }

    protected function getRouter(): \Illuminate\Routing\Router
    {
        return $this->app['router'];
    }

}
