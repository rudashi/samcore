<?php

namespace Totem\SamCore;

use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider;
use Totem\SamCore\App\Traits\TraitServiceProvider;

class SamCoreServiceProvider extends ServiceProvider
{
    use TraitServiceProvider;

    public function getNamespace(): string
    {
        return 'sam-core';
    }

    public function boot(): void
    {
        $this->setLocale();
        $this->setBladeDirectives();

        $this->loadRoutesFrom(__DIR__ . '/routes/api.php');
        $this->loadMigrationsFrom(__DIR__ . '/database/migrations');
    }

    public function register(): void
    {
        $this->configureBinding([
            \Totem\SamCore\App\Repositories\Contracts\FileInterfaceRepository::class => \Totem\SamCore\App\Repositories\FileRepository::class,
        ]);
        $this->registerRoutePattern('id', '[0-9]+');
        $this->registerRoutePattern('uuid', '[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}');
    }

    private function setLocale(): void
    {
        \Carbon\Carbon::setLocale(config('app.faker_locale'));
        setlocale(LC_TIME, config('app.faker_locale') . '.utf8');
    }

    private function setBladeDirectives(): void
    {
        Blade::directive('nl2br', static function ($expression) {
            return "<?php echo nl2br(str_replace(' ', ' &nbsp;', e($expression))); ?>";
        });

        Blade::directive('svg', static function ($arguments) {

            [$path, $class] = array_pad(explode(',', trim($arguments, '() ')), 2, '');
            $path = trim($path, "' ");
            $class = trim($class, "' ");

            $svg = new \DOMDocument();
            $svg->load(public_path($path));
            $svg->documentElement->setAttribute('class', $class);

            return $svg->saveXML($svg->documentElement);

        });
    }

}
