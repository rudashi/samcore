<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFilesTable extends Migration
{

    public function up() : void
    {
        try {
            Schema::create('files', function (Blueprint $table) {
                $table->increments('id');
                $table->timestamps();
                $table->string('name');
                $table->string('original_name');
                $table->string('extension');
                $table->uuidMorphs('attachable');
                $table->softDeletes();
            });
        } catch (PDOException $ex) {
            $this->down();
            throw $ex;
        }
    }

    public function down() : void
    {
        Schema::dropIfExists('files');
    }

}