<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterMorphIdColumnInFilesTable extends Migration
{

    public function up() : void
    {
        Schema::table('files', function (Blueprint $table) {
            $table->string('attachable_type')->nullable()->change();
            $table->string('attachable_id', 36)->nullable()->change();
        });
    }

}