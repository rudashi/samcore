<?php

use Illuminate\Support\Facades\Route;
use Totem\SamCore\App\Controllers\FileController;

Route::group(['middleware' => 'api', 'prefix' => 'api' ], static function() {

    Route::middleware(config('sam-admin.guard-api'))->group(static function() {

        Route::post('file', [FileController::class, 'upload']);
        Route::delete('file/{fileId}', [FileController::class, 'remove'])->where(['fileId' => '[0-9]+']);

    });

});
