<?php

namespace Tests;

use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\LazyCollection;
use Totem\SamCore\App\Services\CSVService;
use Illuminate\Foundation\Testing\WithFaker;

class CSVServiceTest extends TestCase
{
    use WithFaker;

    private CSVService $csvService;

    protected function setUp(): void
    {
        $this->csvService = new CSVService();

        parent::setUp();
    }

    public function test_download_CSV(): void
    {
        $items = $this->makeCollection();

        $response = $this->getContent($this->csvService->streamDownload($items, 'download.csv'));

        self::assertNotEmpty($response);

        foreach ($items as $item) {
            self::assertStringContainsString($item['email'], $response);
        }
    }

    public function test_lazy_collection_download_CSV(): void
    {
        $items = $this->makeCollection(1000);
        $collectionLazy = LazyCollection::make($items);

        $response = $this->getContent($this->csvService->streamDownload($collectionLazy, 'download.csv'));

        self::assertNotEmpty($response);

        foreach ($items as $item) {
            self::assertStringContainsString($item['email'], $response);
        }
    }

    public function test_download_CSV_with_custom_headers(): void
    {
        $headers = ['id', 'address'];
        $items = $this->makeCollection();

        $response = $this->getContent(
            $this->csvService->setRowHeaders($headers)->streamDownload($items, 'download.csv')
        );

        self::assertNotEmpty($response);
        self::assertStringContainsString(implode($this->csvService::DELIMITER, $headers), $response);

        foreach ($items as $item) {
            self::assertStringContainsString($item['email'], $response);
        }
    }

    public function test_download_CSV_with_custom_delimiter(): void
    {
        $delimiter = ',';
        $items = $this->makeCollection();

        $response = $this->getContent(
            $this->csvService->setDelimiter($delimiter)->streamDownload($items, 'download.csv')
        );

        self::assertNotEmpty($response);
        self::assertStringContainsString(implode($delimiter, ['uuid', 'email']), $response);

        foreach ($items as $item) {
            self::assertStringContainsString($item['email'], $response);
        }
    }

    public function test_download_CSV_as_static_method(): void
    {
        $items = $this->makeCollection(5);

        $response = CSVService::download($items, 'download-static');
        $response->deleteFileAfterSend();
        $content = $this->getContent($response);

        self::assertEquals(200, $response->getStatusCode());
        self::assertEquals('attachment; filename=download-static.csv', $response->headers->get('content-disposition'));
        self::assertStringContainsString(implode($this->csvService::DELIMITER, ['uuid', 'email']), $content);

        foreach ($items as $item) {
            self::assertStringContainsString($item['email'], $content);
        }
    }

    private function getContent(Response $response): string
    {
        //START Capture Streamed Output...
        ob_start();
        $response->sendContent();
        return (string) ob_get_clean();
        //END Capture Streamed Output...
    }

    private function makeCollection($total = 20): array
    {
        $count = 0;
        return LazyCollection::make(function () use (&$count, $total) {
            while ($count < $total) {
                $count++;
                yield [
                    'uuid'  => $this->faker->uuid,
                    'email' => $this->faker->email,
                ];
            }
        })->toArray();
    }

}
