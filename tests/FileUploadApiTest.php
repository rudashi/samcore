<?php

namespace Tests;

use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Totem\SamAdmin\Testing\AttachAdminJwtToken;

class FileUploadApiTest extends TestCase
{

    use AttachAdminJwtToken;

    public const FAKE_DISK = 'test';

    public function test_upload_image_file(): void
    {
        $path = 'testa';
        $file = $this->createFakeFile();
        self::assertEquals([], Storage::disk(self::FAKE_DISK)->allFiles());

        $response = $this->post('/api/file', [
            'file_data' =>  $file,
            'path' => $path
        ])
            ->assertCreated()
            ->assertJsonFragment([
                'original_name' => $file->name,
                'extension' => $file->extension(),
                'attachable_type' => null,
                'attachable_id' => null,
            ]);

        $file_uploaded = $response->decodeResponseJson('data.name');
        Storage::disk('public')->assertExists($file_uploaded);
        Storage::disk('public')->deleteDirectory($path);
    }

    public function test_failed_verification_file(): void
    {
        $this->post('/api/file', [])
            ->assertStatus(422)
            ->assertJsonFragment([
                'error' => [
                    'code' => 422,
                    'message' => [
                        'file_data' => ['The `file_data` field is required.']
                    ]
                ],
            ]);
    }

    public function test_failed_verification_path_file(): void
    {
        $file = $this->createFakeFile();

        $this->post('/api/file',[
            'file_data' => $file
        ])
            ->assertStatus(400)
            ->assertJsonFragment([
                'error' => [
                    'code' => 400,
                    'message' => __('The :attribute field is required.', ['attribute' => 'path'])
                ],
            ]);
        self::assertEquals([], Storage::disk(self::FAKE_DISK)->allFiles());
    }

    public function test_failed_verification_file_mime(): void
    {
        $this->post('/api/file', ['file_data' => 'file'])
            ->assertStatus(422)
            ->assertJsonFragment([
                'error' => [
                    'code' => 422,
                    'message' => [
                        'file_data' => ['The `file_data` must be a file of type: jpeg, png, jpg, gif, pdf.']
                    ]
                ],
            ]);
    }

    public function test_remove_image_file(): void
    {
        $path = 'testa';
        $file = $this->createFakeFile();
        self::assertEquals([], Storage::disk(self::FAKE_DISK)->allFiles());

        $file_uploaded = $this->post('/api/file', [
            'file_data' =>  $file,
            'path' => $path
        ])->decodeResponseJson('data');

        $this->withToken()->delete("/api/file/{$file_uploaded['id']}")
            ->assertOk()
            ->assertJsonFragment([
                'id' => $file_uploaded['id'],
                'name' => $file_uploaded['name'],
                'type' => $file_uploaded['type'],
                'original_name' => $file_uploaded['original_name'],
                'extension' => $file_uploaded['extension'],
                'attachable_type' => null,
                'attachable_id' => null,
                'preview' => $file_uploaded['preview'],
            ]);

        $this->assertSoftDeleted('files', [
            'id' => $file_uploaded['id']
        ]);

        Storage::disk('public')->assertExists($file_uploaded['name']);
        Storage::disk('public')->deleteDirectory($path);
    }

    private function createFakeFile(string $filename = 'avatar.jpg'): \Illuminate\Http\Testing\File
    {
        Storage::fake(self::FAKE_DISK);

        return UploadedFile::fake()->image($filename);
    }

}
